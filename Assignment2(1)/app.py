from flask import Flask, request, render_template

app = Flask(__name__)

addresses = []
people = []

class Address:
    ''' class creating an address '''
    def __init__(self,name,street,city,province):
        self.name=name
        self.street=street
        self.city=city
        self.province=province
    def __str__(self):
        return f"{self.name}: {self.street}, {self.city}, {self.province}"

class Person:
    '''creates a new person'''
    def __init__(self,first_name,last_name,age):
        self.first_name=first_name
        self.last_name=last_name
        self.age=age
    def __str__(self):
        return f'{self.first_name} {self.last_name}, {self.age} '
class Student(Person):
    '''creates a new student'''
    def __init__(self,first_name,last_name,age,grade):
        super().__init__(first_name,last_name,age)

        if int(grade) <0 or int(grade) > 100:
            raise ValueError("grade can only be between 0 and 100!")
        self.grade=grade
    def map_grade(self,grade):
        '''gives a letter grade representing the grade number'''
        if int(grade) >= 95:
            letter_grade = 'A'
        elif int(grade) >=80:
            letter_grade='B'
        elif int(grade) >=70:
            letter_grade = 'C'
        elif int(grade) >=60:
            letter_grade= 'D'
        else:
            letter_grade= 'F'
        return letter_grade
    def __str__(self):
        letter_grade=self.map_grade(self.grade)
        return f'{super().__str__()}, {letter_grade}'

@app.route("/")
def home():
    '''used for rendering home.html'''
    return render_template('home.html')

@app.route("/newaddress")
def new_address():
    '''used for rendering newaddress.html'''
    return render_template('newaddress.html')

@app.route("/listaddresses")
def list_addresses():
    '''used for rendering listaddress.html'''
    return render_template('listaddress.html',address_list=addresses)

@app.route("/newaddress", methods=['POST'])
def new_address_post():
    '''used for receiving values from new_address_post'''
    name = request.form['name']
    street = request.form['street']
    city = request.form['city']
    province = request.form['province']
    if len(name) ==0 or len(street)==0 or len(city)==0 or len(province)==0:
        return'''
            <h2>Invalid Input. Try again.
            <form method="post">
            <input name="name" placeholder="Name" />
            <input name="street" placeholder="Street" />
            <input name="city" placeholder="City" />
            <input name="province" placeholder="Province" />
            <input type="submit"/>
        </form>'''
    new_addy=Address(name,street,city,province)
    addresses.append(new_addy)
    return f"<h1>{str(new_addy)}</h1>"

@app.route("/listpeople")
def list_people():
    '''used to render listpeople.html'''
    for person in people:
        if isinstance(person,Student):
            person.isStudent=True
        else:
            person.isStudent=False
    return render_template('listpeople.html',people_list=people)

@app.route("/newperson")
def new_person():
    '''used to render new person'''
    return render_template('newperson.html')

@app.route("/newperson", methods=['POST'])
def new_person_post():
    '''used to receive values from new person post'''
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    age = request.form['age']
    if age.isnumeric() is not True or len(first_name) == 0 or len(last_name) == 0:
        return '''
            <h2>Invaid Input! Try again. </h2>
            <form method="post">
            <input name="first_name" placeholder="First Name" />
            <input name="last_name" placeholder="Last Name" />
            <input name="age" placeholder="Age" />
            <input type="submit"/>
            </form> 
            '''
    new_person_character=Person(first_name,last_name,age)
    people.append(new_person_character)
    return f'<h1>{str(new_person_character)}</h1>'

@app.route("/newstudent")
def newstudent():
    '''used to render newstudent.html'''
    return render_template('newstudent.html')

@app.route("/newstudent", methods=['POST'])
def newstudent_post():
    '''used to receive student variables'''
    first_name= request.form['first_name']
    last_name = request.form['last_name']
    age = request.form['age']
    grade= request.form['grade']
    if age.isnumeric() is not True or len(first_name) == 0 or len(last_name) == 0 or grade.isnumeric() is not True:
        return '''
            <h2>Invalid Input.Try again.
            <form method="post">
            <input name="first_name" placeholder="First Name" />
            <input name="last_name" placeholder="Last Name" />
            <input name="age" placeholder="Age" />
            <input name="grade" placeholder="Grade" />
            <input type="submit"/>
        </form>'''
    new_student = Student(first_name,last_name,age,grade)
    people.append(new_student)
    return f'<h1>{str(new_student)}</h1>'

if __name__ == '__main__':
    app.run(port=5002, debug=True)
